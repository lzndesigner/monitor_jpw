<p><small><b>Campos obrigatórios (<span class="text-danger">*</span>)</b></small></p>

<input type="hidden" id="customer_id" name="customer_id" value="{{isset($customer_id) ? $customer_id : 0}}">

<fieldset>
  <h6>Dados do Serviço</h6>
  <div class="form-row align-items-center">
    <div class="col-xs-6 col-md-6">
      <div class="form-group">
        <label for="service_id" class="col-form-label">Produto/Serviço (<span class="text-danger">*</span>):</label>
        <select name="service_id" id="service_id" class="form-control">
        <option disabled selected>Selecione um Produto/Serviço</option>
        @if(isset($getServices))
            @foreach($getServices as $getService)
              @if(isset($result->service_id))
                @if($getService->id == $result->service_id)
                  <option value="{{ $getService->id }}" selected>{{ $getService->name }}</option>
                @else
                  <option value="{{ $getService->id }}">{{ $getService->name }}</option>
                @endif
              @else
                  <option value="{{ $getService->id }}">{{ $getService->name }}</option>
              @endif
            @endforeach
          @endif
        </select>
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-6 col-md-4">
      <div class="form-group">
        <label for="dominio" class="col-form-label">Domínio:</label>
        <input type="text" id="dominio" name="dominio" class="form-control" placeholder="Domínio" value="{{isset($result->dominio) ? $result->dominio : ''}}">
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-6 col-md-2">
      <div class="form-group">
        <label for="status">Status</label>
        @if(isset($result->status))
        @if($result->status == 'Ativo')
        <div class="custom-control custom-switch checked-success">
          <input type="checkbox" class="custom-control-input" name="status" id="status" value="Ativo" checked>
          <label class="custom-control-label" for="status" checked>Ativo</label>
        </div>
        @elseif($result->status == 'Pendente')
        <div class="custom-control custom-switch checked-info">
          <input type="checkbox" class="custom-control-input" name="status" id="status" value="Ativo">
          <label class="custom-control-label" for="status">Pendente</label>
        </div>
        @else
        <div class="custom-control custom-switch checked-danger">
          <input type="checkbox" class="custom-control-input" name="status" id="status" value="Cancelado">
          <label class="custom-control-label" for="status">Cancelado</label>
        </div>
        @endif
        @else
        <div class="custom-control custom-switch checked-success">
          <input type="checkbox" class="custom-control-input" name="status" id="status" value="Ativo" checked>
          <label class="custom-control-label" for="status" checked>Ativo</label>
        </div>
        @endif
      </div>
    </div><!-- col -->
  </div><!-- form-row -->

  <div class="form-row">
    <div class="col-xs-3 col-md-3">
      <div class="form-group">
        <label for="date_start" class="col-form-label">Data Início (<span class="text-danger">*</span>):</label>
        <input type="text" id="date_start" name="date_start" class="form-control" placeholder="Data Início" value="{{isset($result->date_start) ? $result->date_start : \Carbon\Carbon::now()->format('Y-m-d') }}" required>
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-3 col-md-3">
      <div class="form-group">
        <label for="date_end" class="col-form-label">Data Vencimento (<span class="text-danger">*</span>):</label>
        <input type="text" id="date_end" name="date_end" class="form-control" placeholder="Data Vencimento" value="{{isset($result->date_end) ? $result->date_end : ''}}" required>
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-3 col-md-3">
      <div class="form-group">
        <label for="price" class="col-form-label">Valor Recorrente (<span class="text-danger">*</span>):</label>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">R$</span>
          </div>
          <input type="text" id="price" name="price" class="form-control formatedPrice" placeholder="100,00" value="{{isset($result->price) ? $result->price : ''}}" required>
        </div>

      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-3 col-md-3">
      <div class="form-group">
        <label for="period" class="col-form-label">Ciclo:</label>
        <select name="period" id="period" class="form-control">
          <option value="mensal" selected>Mensal</option>
          <option value="trimestral">Trimestral</option>
          <option value="anual">Anual</option>
        </select>
      </div><!-- form-group -->
    </div><!-- col -->
  </div><!-- form-row -->
</fieldset>

<fieldset>
  <h6>Dados de Acesso</h6>
  <div class="form-row">
    <div class="col-xs-3 col-md-5">
      <div class="form-group">
        <label for="ftp" class="col-form-label">FTP:</label>
        <input type="text" id="ftp" name="ftp" class="form-control" placeholder="FTP" value="{{isset($result->ftp) ? $result->ftp : ''}}">
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-6 col-md-4">
      <div class="form-group">
        <label for="ftp_login" class="col-form-label">Usuário de Acesso:</label>
        <input type="text" id="ftp_login" name="ftp_login" class="form-control" placeholder="Usuário de Acesso" value="{{isset($result->ftp_login) ? $result->ftp_login : ''}}">
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-3 col-md-3">
      <div class="form-group">
        <label for="ftp_password" class="col-form-label">Senha de Acesso:</label>
        <input type="text" id="ftp_password" name="ftp_password" class="form-control" placeholder="Senha de Acesso" value="{{isset($result->ftp_password) ? $result->ftp_password : ''}}">
      </div><!-- form-group -->
    </div><!-- col -->
  </div><!-- form-row -->
</fieldset>