@extends('backend.base')
@section('title', $titulo)

@section('breadcrumb')
<div class="page-heading">
  <h1>@yield('title') ({{count($results)}})</h1>
  <ul class="breadcrumb">
    <li><a href="{{ route('backend.home') }}">Início</a></li>
    <li><a href="">@yield('title')</a></li>
  </ul>
</div>
@endsection

@section('content')
<section>
  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover mb-0">
          <thead>
            <th width="40px">
              <div class="form-group form-check">
                <input type="checkbox" id="checkAll" value="" class="form-check-input">
              </div>
            </th>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'name', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> N° Atendimento</th>
            <th>Serviços</th>
            <th><a href=""><i class="fa fa-sort"></i></a> Data Checkin</th>
          </thead>
          <tbody>
            <form class="form" id="form-table">
              {{ csrf_field() }}
              @foreach($results as $result)
              <tr>
                <td>
                  <div class="form-group form-check">
                    <input type="checkbox" name="selected[]" value="{{ $result->number }}" class="form-check-input">
                  </div>
                </td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">N° Atendimento:</span> <a href="{{ url('buscar?code='.$result->number) }}">{{ $result->number }}</a></td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Total de Códigos:</span> {{ $result->qtaCodes }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Data Checkin:</span> {{ \Carbon\Carbon::parse($result->date_checkin)->format('d/m/Y') }}</td>
              </tr>
              @endforeach
            </form>
          </tbody>
        </table>
        <div class="d-flex align-items-center mt-3">
          <div class="mr-2"><small>Com os Selecionados:</small></div>
          <div><a href="javascript:;" id="btn-delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i> Remover selecionados</a></div>
          <div class="flex-grow-1">
            {{$results->appends(request()->query())->links() }}
          </div>
        </div><!-- d-flex -->
      </div><!-- table-responsive -->
      <hr>
      <div class="row">
        <div class="col-xs-12">
          <a href="{{ url('checkin-create') }}" class="btn btn-sm btn-thema" data-type="create"><i class="fa fa-plus"></i> Novo Atendimento</a>
        </div>
      </div>


    </div><!-- card-body -->
  </div><!-- card -->
</section>
@endsection

@section('cssPage')
<link rel="stylesheet" href="/general/plugins/sweetalert/sweetalert2.min.css">
@endsection

@section('jsPage')
<script src="/general/plugins/sweetalert/sweetalert2.min.js"></script>
<script>
  $("#checkAll").click(function() {
    $('input:checkbox').not(this).prop('checked', this.checked);
  });

  // Button - Delete - Not action Waiting
  $('#btn-delete').click(function(e) {
    Swal.fire({
      title: 'Desejar remover esse Atendimento?',
      text: "Essa ação irá apagar todos os códigos de barras referente a esse atendimento!",
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, deletar!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: "{{url('atendimento-delete')}}",
          method: 'POST',
          data: $('#form-table').serialize(),
          success: function(data) {
            // Loading page listagem
            location.reload();
          },
          error: function(xhr) {
            if (xhr.status === 422) {
              Swal.fire({
                text: xhr.responseJSON,
                icon: 'warning',
                showClass: {
                  popup: 'animate_animated animate_wobble'
                }
              });
            } else {
              Swal.fire({
                text: xhr.responseJSON,
                icon: 'error',
                showClass: {
                  popup: 'animate_animated animate_wobble'
                }
              });
            }
          }
        });
      }
    });

  });
</script>
@endsection