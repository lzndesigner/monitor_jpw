@extends('backend.base_monitoracao')
@section('title', 'Monitoração')

@section('content')
<section>
  <div class="card card-central">
    <div class="card-body px-0">
      <div class="row box-monitoracao mb-5">
        <div class="col-xs-12 col-md-4">
          <div class="card">
            <div class="card-body text-center p-2">
              <h4 class="card-title">Checkin</h4>
              <p class="mb-1">total: {{count($getCheckins)}}</p>

              <ul class="list-codes">
                @foreach($getCheckins as $getCheckin)
                <li><a href="{{ url('buscar?code='.$getCheckin->number_id) }}" class="code">{{ $getCheckin->number_id }}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
        </div><!-- col-6 -->
        <div class="col-xs-12 col-md-4">
          <div class="card">
            <div class="card-body text-center p-2">
              <h4 class="card-title">Lavagem</h4>
              <p class="mb-1">total: {{count($getLavagens)}}</p>

              <ul class="list-codes">
                @foreach($getLavagens as $getLavagen)
                <li><a href="{{ url('buscar?code='.$getLavagen->number_id) }}" class="code">{{ $getLavagen->number_id }}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
        </div><!-- col-6 -->
        <div class="col-xs-12 col-md-4">
          <div class="card">
            <div class="card-body text-center p-2">
              <h4 class="card-title">Secagem</h4>
              <p class="mb-1">total: {{count($getSecagens)}}</p>

              <ul class="list-codes">
                @foreach($getSecagens as $getSecagen)
                <li><a href="{{ url('buscar?code='.$getSecagen->number_id) }}" class="code">{{ $getSecagen->number_id }}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
        </div><!-- col-6 -->
      </div><!-- row -->



      <div class="row box-monitoracao mt-5">

        <div class="col-xs-12 col-md-4">
          <div class="card">
            <div class="card-body text-center p-2">
              <h4 class="card-title">Acabamento</h4>
              <p class="mb-1">total: {{count($getAcabamentos)}}</p>

              <ul class="list-codes">
                @foreach($getAcabamentos as $getAcabamento)
                <li><a href="{{ url('buscar?code='.$getAcabamento->number_id) }}" class="code">{{ $getAcabamento->number_id }}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
        </div><!-- col-6 -->
        <div class="col-xs-12 col-md-4">
          <div class="card">
            <div class="card-body text-center p-2">
              <h4 class="card-title">Finalizado</h4>
              <p class="mb-1">total: {{count($getFinalizados)}}</p>

              <ul class="list-codes">
                @foreach($getFinalizados as $getFinalizado)
                <li><a href="{{ url('buscar?code='.$getFinalizado->number_id) }}" class="code">{{ $getFinalizado->number_id }}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
        </div><!-- col-6 -->

        <div class="col-xs-12 col-md-4">
          <div class="card">
            <div class="card-body text-center p-2">
              <h4 class="card-title">Aguardando Retirada</h4>
              <p class="mb-1">total: {{count($getWaitings)}}</p>

              <ul class="list-codes">
                @foreach($getWaitings as $getWaiting)
                <li><a href="{{ url('buscar?code='.$getWaiting->number_id) }}" class="code">{{ $getWaiting->number_id }}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
        </div><!-- col-6 -->
      </div><!-- row -->


    </div><!-- card -->
  </div><!-- card -->
</section>
@endsection

@section('column_right')
<div class="box-atrasados">
  <div class="card">
    <div class="card-body text-center p-2">
      <div class="sub-heading">
        <h2>Atrasados</h2>
      </div>

      <div class="card mt-5">
        <div class="card-body card-warning p-2">
          <h4 class="card-title">+ de 7 dias</h4>

          <ul class="list-codes">
            @foreach($getSevendays as $getSevenday)
            <li><a href="{{ url('buscar?code='.$getSevenday->number_id) }}" class="code">{{ $getSevenday->number_id }}</a></li>
            @endforeach
          </ul>

        </div><!-- card -->
      </div><!-- card - warning 7 dias -->

      <div class="card mt-5">
        <div class="card-body card-danger p-2">
          <h4 class="card-title">+ de 9 dias</h4>

          <ul class="list-codes">
            @foreach($getNinedays as $getNineday)
            <li><a href="{{ url('buscar?code='.$getNineday->number_id) }}" class="code">{{ $getNineday->number_id }}</a></li>
            @endforeach
          </ul>

        </div><!-- card -->
      </div><!-- card - danger 9 dias -->

      <div class="card mt-5">
        <div class="card-body card-dark p-2">
          <h4 class="card-title">+ de 10 dias</h4>

          <ul class="list-codes">
            @foreach($getTendays as $getTenday)
            <li><a href="{{ url('buscar?code='.$getTenday->number_id) }}" class="code">{{ $getTenday->number_id }}</a></li>
            @endforeach
          </ul>

        </div><!-- card -->
      </div><!-- card - dark 10 dias -->


    </div><!-- card -->
  </div><!-- card -->
</div><!-- box-atrasados -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection