<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/************************/
// Routes Frontend
Route::get('/', function () {
    $url = "https://lavanderiaalves.com.br/";
    return Redirect::to($url);
});

/************************/
// Routes Backend
Auth::routes();
// Route::prefix('admin')->group(function () {
Route::get('/', 'Backend\HomeController@index')->name('backend.home');

//Customers
Route::get('/customers', 'Backend\CustomerController@index')->name('backend.customers');
Route::get('/customers-create', 'Backend\CustomerController@create')->name('backend.customers.create');
Route::post('/customers-store', 'Backend\CustomerController@store')->name('backend.customers.store');
Route::get('/customers-edit/{id}', 'Backend\CustomerController@edit')->name('backend.customers.edit');
Route::put('/customers-update/{id}', 'Backend\CustomerController@update')->name('backend.customers.update');
Route::get('/customers-details/{id}', 'Backend\CustomerController@show')->name('backend.customers.show');
Route::delete('/customers-delete', 'Backend\CustomerController@destroy')->name('backend.customers.delete');

// Auxiliares
Route::get('/checkcode/{codebar}', 'Backend\HomeController@checkcode')->name('backend.checkcode');
Route::get('/checkcodestep/{codebar}', 'Backend\HomeController@checkcodestep')->name('backend.checkcodestep');
Route::get('/buscar', 'Backend\HomeController@search')->name('backend.search');


Route::get('/atendimentos', 'Backend\AttendanceController@index')->name('backend.atendimentos');
Route::post('/atendimento-delete', 'Backend\AttendanceController@destroyAttendance')->name('backend.atendimento.delete');
Route::post('/codebar-delete', 'Backend\HomeController@destroyCode')->name('backend.codebar.delete');

//Steps - CheckIn
Route::get('/checkin', 'Backend\Steps\CheckinController@index')->name('backend.steps.checkin');
Route::get('/checkin-create', 'Backend\Steps\CheckinController@create')->name('backend.steps.checkin.create');
Route::post('/checkin-store', 'Backend\Steps\CheckinController@store')->name('backend.steps.checkin.store');
Route::delete('/checkin-delete', 'Backend\Steps\CheckinController@destroy')->name('backend.steps.checkin.delete');

//Steps - Lavagem
Route::get('/lavagem', 'Backend\Steps\LavagemController@index')->name('backend.steps.lavagem');
Route::get('/lavagem-create', 'Backend\Steps\LavagemController@create')->name('backend.steps.lavagem.create');
Route::post('/lavagem-store', 'Backend\Steps\LavagemController@store')->name('backend.steps.lavagem.store');
Route::delete('/lavagem-delete', 'Backend\Steps\LavagemController@destroy')->name('backend.steps.lavagem.delete');

//Steps - Secagem
Route::get('/secagem', 'Backend\Steps\SecagemController@index')->name('backend.steps.secagem');
Route::get('/secagem-create', 'Backend\Steps\SecagemController@create')->name('backend.steps.secagem.create');
Route::post('/secagem-store', 'Backend\Steps\SecagemController@store')->name('backend.steps.secagem.store');
Route::delete('/secagem-delete', 'Backend\Steps\SecagemController@destroy')->name('backend.steps.secagem.delete');

//Steps - Acabamento
Route::get('/acabamento', 'Backend\Steps\AcabamentoController@index')->name('backend.steps.acabamento');
Route::get('/acabamento-create', 'Backend\Steps\AcabamentoController@create')->name('backend.steps.acabamento.create');
Route::post('/acabamento-store', 'Backend\Steps\AcabamentoController@store')->name('backend.steps.acabamento.store');
Route::delete('/acabamento-delete', 'Backend\Steps\AcabamentoController@destroy')->name('backend.steps.acabamento.delete');

//Steps - Finalizado
Route::get('/finalizado', 'Backend\Steps\FinalizadoController@index')->name('backend.steps.finalizado');
Route::get('/finalizado-create', 'Backend\Steps\FinalizadoController@create')->name('backend.steps.finalizado.create');
Route::post('/finalizado-store', 'Backend\Steps\FinalizadoController@store')->name('backend.steps.finalizado.store');
Route::delete('/finalizado-delete', 'Backend\Steps\FinalizadoController@destroy')->name('backend.steps.finalizado.delete');

//Steps - Checkout
Route::get('/checkout', 'Backend\Steps\CheckoutController@index')->name('backend.steps.checkout');
Route::get('/checkout-create', 'Backend\Steps\CheckoutController@create')->name('backend.steps.checkout.create');
Route::post('/checkout-store', 'Backend\Steps\CheckoutController@store')->name('backend.steps.checkout.store');
Route::delete('/checkout-delete', 'Backend\Steps\CheckoutController@destroy')->name('backend.steps.checkout.delete');

//Steps - Waiting
Route::get('/waiting', 'Backend\Steps\WaitingController@index')->name('backend.steps.waiting');
Route::get('/waiting-create', 'Backend\Steps\WaitingController@create')->name('backend.steps.waiting.create');
Route::post('/waiting-store', 'Backend\Steps\WaitingController@store')->name('backend.steps.waiting.store');
Route::delete('/waiting-delete', 'Backend\Steps\WaitingController@destroy')->name('backend.steps.waiting.delete');

Route::get('/monitoracao', 'Backend\HomeController@monitoracao')->name('backend.monitoracao');


Route::get('/settings', 'Backend\HomeController@settings')->name('backend.settings');
    
// });