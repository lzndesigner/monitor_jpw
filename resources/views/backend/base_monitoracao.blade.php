<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <base href="{{Request::url()}}" />
  <title>@yield('title') - Painel de Controle</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
  <!-- Include Fontawesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{ asset('general/css/bootstrap.css?').rand() }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('backend/css/template.css?').rand() }}" type="text/css" />
  @yield('cssPage')
</head>

<body>
  <div class="page-fullheight">
    <header class="mb-3">
      <div class="topHeader mb-2 py-1">
        <a href="/" class="btn btn-xs btn-thema"><i class="fa fa-arrow-left"></i></a>
        <a href="javascript:;" class="openMenu btn btn-xs btn-thema"><i class="fa fa-bars"></i> Menu</a>
      </div><!-- topHeader -->

      <div class="topCenter hide">
        <div class="d-flex justify-content-between align-items-center px-4">
          <div class="">
            <div id="logo">

            </div><!-- logo -->
          </div><!-- flex-fill -->

          <div class="col-md-6 col-lg-3 col-code">
            <div class="card">
              <div class="card-body">
                <form action="{{ url('buscar') }}" id="form-search" class="d-flex">
                  <div class="flex-grow-1 mr-2"><input type="text" class="form-control inputSearchCode" id="code" name="code" placeholder="N° ou Código de Barras" autofocus></div>
                  <div class=""><button type="submit" class="btn btn-thema" id="btn-search"><i class="fa fa-search"></i> Buscar</button></div>
                </form>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- flex-fill -->
        </div><!-- d-flex -->
      </div><!-- topCenter -->
    </header>

    <div class="container-fluid">
      <div class="row">
        <nav class="navMenu navMenu-monitor sidebar collapse">
          @include('backend.includes.menuprincipal')
        </nav>

        <main class="col-sm-12 col-md-10 ml-sm-auto px-md-4 mb-3">
          @yield('breadcrumb')
          @yield('content')
        </main>
        <aside class="col-sm-12 col-md-2 d-md-block sidebar">
          @yield('column_right')
        </aside>
      </div>
    </div>
  </div><!-- boxed -->

  <script type="text/javascript" src="{{ asset('general/js/jquery-3.5.1.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('general/js/bootstrap.bundle.js') }}"></script>
  <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
  <script type="text/javascript" src="{{ asset('general/js/jquery.maskedinput.js?1') }}"></script>
  <script src="{{ mix('js/app.js') }}"></script>
  @yield('jsPage')
  <script type="text/javascript" src="{{ asset('backend/js/functions.js?').rand() }}"></script>

  <script>

    // setTimeout(function() {
    //     location.reload();
    // }, 60000);


    $(document).on('click', '.openMenu', function(e) {
      e.preventDefault();
      $('.navMenu').toggleClass('collapse', 15000);
    });

    // Count e Limit catacteres - meta keywords
    $(document).on("input", ".inputSearchCode:last-child", function() {
      var inicio = 0;
      var limite = 13;
      var informativo = "/" + limite + " caracteres restantes.";
      var caracteresDigitados = $(this).val().length;
      var caracteresAtuais = caracteresDigitados;
      var caracteresRestantes = limite - caracteresDigitados;

      if (caracteresRestantes <= 0) {
        var input = $(".inputSearchCode");
        var newvalue = input.val().substring(0, 13);
        input.val(newvalue);
      }
    });
  </script>
</body>

</html>
