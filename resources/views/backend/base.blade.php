<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <base href="{{Request::url()}}" />
  <title>@yield('title') - Painel de Controle</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
  <!-- Include Fontawesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{ asset('general/css/bootstrap.css?').rand() }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('backend/css/template.css?').rand() }}" type="text/css" />
  @yield('cssPage')
</head>

<body>
  <div class="">
    <header>
      <div class="topHeader">
        <div class="d-flex justify-content-between align-items-center px-2">
          <div class="d-block d-sm-block d-md-none">
            <a href="javascript:;" class="openMenu btn btn-sm btn-thema"><i class="fa fa-bars"></i> Menu</a>
          </div>
          <div class="d-none d-sm-none d-md-block">
            <h2>Painel de Controle</h2>
          </div><!-- flex-fill -->
          <div class="">
            <a href="{{ route('backend.steps.checkin.create') }}" data-toggle="tooltip" data-placement="bottom" title="Novo Atendimento" class="btn btn-sm btn-thema">Novo Atendimento</a>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-toggle="tooltip" data-placement="bottom" title="Deslogar-se" class="btn btn-sm btn-thema ml-2"><i class="fa fa-sign-out"></i></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </div><!-- flex-fill -->
        </div><!-- dflex -->
      </div><!-- topHeader -->

      <div class="topCenter">
        <div class="d-flex justify-content-between align-items-center px-4">
          <div class="d-none d-sm-none d-md-block">
            <div id="logo">
              <a href="javascript:;">
                <img src="{{ asset('general/images/logo.png') }}" alt="Lavanderia Alves" class="img-fluid" style="width:190px;">
                <img src="{{ asset('general/images/logo_stylo.png') }}" alt="Stylo Lavanderia" class="img-fluid" style="width:190px;">
              </a>
            </div><!-- logo -->
          </div><!-- flex-fill -->

          <div class="col-md-4 col-code">
            <div class="card">
              <div class="card-body">
                <p class="mb-1 d-none d-sm-none d-md-inline-block"><b>N° de Atendimento ou Código de Barras:</b></p>
                <form action="{{ url('buscar') }}" id="form-search" class="d-flex">
                  <div class="flex-grow-1 mr-2"><input type="tel" class="form-control inputSearchCode" id="code" name="code" placeholder="N° ou Código de Barras" autofocus></div>
                  <div class=""><button type="submit" class="btn btn-thema" id="btn-search"><i class="fa fa-search"></i> <span class="d-none d-sm-none d-md-inline-block">Buscar</span></button></div>
                </form>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- flex-fill -->
        </div><!-- d-flex -->
      </div><!-- topCenter -->
    </header>

    <div class="container-fluid">
      <div class="row">
        <aside class="col-md-3 col-lg-2">
          <nav class="navMenu d-md-block sidebar collapse">
            @include('backend.includes.menuprincipal')
          </nav>
        </aside>

        <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
          @yield('breadcrumb')
          @yield('content')
        </main>
      </div>
    </div>

    <footer>
      <p>Todos os Direitos Reservados</p>
    </footer>
  </div><!-- boxed -->

  <script type="text/javascript" src="{{ asset('general/js/jquery-3.5.1.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('general/js/bootstrap.bundle.js') }}"></script>
  <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
  <script type="text/javascript" src="{{ asset('general/js/jquery.maskedinput.js?1') }}"></script>
  @yield('jsPage')
  <script type="text/javascript" src="{{ asset('backend/js/functions.js?').rand() }}"></script>

  <script>
    $(document).on('click', '.openMenu', function(e) {
      e.preventDefault();
      $('.navMenu').toggleClass('collapse', 15000);
    });

    // Count e Limit catacteres - meta keywords
    $(document).on("click", ".inputSearchCode:last-child", function() {
      var inicio = 0;
      var limite = 13;
      var informativo = "/" + limite + " caracteres restantes.";
      var caracteresDigitados = $(this).val().length;
      var caracteresAtuais = caracteresDigitados;
      var caracteresRestantes = limite - caracteresDigitados;

      if (caracteresRestantes <= 0) {
        var input = $(".inputSearchCode");
        var newvalue = input.val().substring(0, 13);
        input.val(newvalue);
      }
    });
  </script>
</body>

</html>