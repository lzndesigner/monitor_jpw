@extends('backend.base')
@section('title', 'Pagina Inicial')

@section('breadcrumb')
<div class="page-heading">
  <h1>@yield('title')</h1>
  <ul class="breadcrumb">
    <li><a href="{{ route('backend.home') }}">Início</a></li>
    <li><a href="">@yield('title')</a></li>
  </ul>
</div>
@endsection

@section('content')
<section>
  <div class="d-flex flex-column flex-sm-column flex-lg-row">
    <div class="flex-fill mr-4 mb-2 mb-sm-2 mb-md-2 mb-lg-0">
      <div class="card card-widget-home">
        <div class="card-body">
          <div class="icon"><a href="{{ url('/checkin') }}">{{ $countStepCheckin }}</a></div>
          <h4><a href="{{ url('/checkin') }}">Check-In</a></h4>
        </div>
      </div>
    </div><!-- flex-fill mr-4 mb-2 mb-sm-2 mb-md-2 mb-lg-0 -->
    <div class="flex-fill mr-4 mb-2 mb-sm-2 mb-md-2 mb-lg-0">
      <div class="card card-widget-home">
        <div class="card-body">
          <div class="icon"><a href="{{ url('/lavagem') }}">{{ $countStepLavagem }}</a></div>
          <h4><a href="{{ url('/lavagem') }}">Lavagem</a></h4>
        </div>
      </div>
    </div><!-- flex-fill mr-4 mb-2 mb-sm-2 mb-md-2 mb-lg-0 -->
    <div class="flex-fill mr-4 mb-2 mb-sm-2 mb-md-2 mb-lg-0">
      <div class="card card-widget-home">
        <div class="card-body">
          <div class="icon"><a href="{{ url('/secagem') }}">{{ $countStepSecagem }}</a></div>
          <h4><a href="{{ url('/secagem') }}">Secagem</a></h4>
        </div>
      </div>
    </div><!-- flex-fill mr-4 mb-2 mb-sm-2 mb-md-2 mb-lg-0 -->
    <div class="flex-fill mr-4 mb-2 mb-sm-2 mb-md-2 mb-lg-0">
      <div class="card card-widget-home">
        <div class="card-body">
          <div class="icon"><a href="{{ url('/acabamento') }}">{{ $countStepAcabamento }}</a></div>
          <h4><a href="{{ url('/acabamento') }}">Acabamento</a></h4>
        </div>
      </div>
    </div><!-- flex-fill mr-4 mb-2 mb-sm-2 mb-md-2 mb-lg-0 -->
    <div class="flex-fill mr-4 mb-2 mb-sm-2 mb-md-2 mb-lg-0">
      <div class="card card-widget-home">
        <div class="card-body">
          <div class="icon"><a href="{{ url('/finalizado') }}">{{ $countStepFinalizado }}</a></div>
          <h4><a href="{{ url('/finalizado') }}">Finalizado</a></h4>
        </div>
      </div>
    </div><!-- flex-fill -->
    <div class="flex-fill">
      <div class="card card-widget-home">
        <div class="card-body">
          <div class="icon"><a href="{{ url('/waiting') }}">{{ $countStepWaiting }}</a></div>
          <h4><a href="{{ url('/waiting') }}">Aguardando</a></h4>
        </div>
      </div>
    </div><!-- flex-fill -->
  </div><!-- d-flex -->
  <p class="mt-3">Total de Atendimentos <span class="badge badge-thema">{{$countTotal}}</span></p>
</section>
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection