@extends('backend.base')
@section('title', $titulo)

@section('breadcrumb')
<div class="page-heading">
  <h1>@yield('title')</h1>
  <ul class="breadcrumb">
    <li><a href="{{ route('backend.home') }}">Início</a></li>
    <li><a href="">@yield('title')</a></li>
  </ul>
</div>
@endsection

@section('content')
<section>
  <div class="card">
    <div class="card-body" id="content-page">
      @include('backend.steps.checkin.form')
    </div><!-- card-body -->
  </div><!-- card -->
</section>
@endsection

@section('cssPage')
<link rel="stylesheet" href="/general/plugins/sweetalert/sweetalert2.min.css">
@endsection

@section('jsPage')
<script src="/general/plugins/sweetalert/sweetalert2.min.js"></script>
<script>
$('#form-request').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

  // First input focus
  $('#number').last().focus();

  // Count e Limit catacteres - meta keywords
  var atual = $('.codeInput').last().val().length;
  $(".CountCaracteresName").text(atual + '/13 caracteres restantes.');

  $(document).on("input", ".codeInput:last-child", function() {
    var inicio = 0;
    var limite = 13;
    var informativo = "/" + limite + " caracteres restantes.";
    var caracteresDigitados = $(this).val().length;
    var caracteresAtuais = caracteresDigitados;
    var caracteresRestantes = limite - caracteresDigitados;

    if (caracteresRestantes <= 0) {
      var comentario = $(".codeInput:last-child").val();
      // console.log(comentario);
      $(".CountCaracteresName").text('13' + informativo);
    } else {
      $(".CountCaracteresName").text(caracteresAtuais + informativo);
    }
  });



  function validCode(input) {
    var maxTotalCode = 13; //max 13 digitos
    var codebar = input.val();
    var countLenght = input.val().length;



    var inputs = $("input.codeInput")
      .map(function() {
        return $(this).val();
      }).get();

    var uniqueCodes = [];
    $.each(inputs, function(i, el) {
      if ($.inArray(el, uniqueCodes) === -1) {
        uniqueCodes.push(el);
      } else {
        $(".codeInput").each(function() {
          if (el == $(this).val()) {
            $(this).addClass('hasError').addClass('important');
            $(".codeInput.hasSuccess.hasError.important").each(function() {
              $(this).parent().parent().find('a').removeClass('hide');
            });
          }
        });
      }
    });


    if (input.val().length > maxTotalCode) {
      var newvalue = input.val().substring(0, maxTotalCode);
      input.val(newvalue);
    }

    // Aqui entra em ação quando digita os maxTotalCode
    if (countLenght == maxTotalCode) {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
      });

      var url = `{{url('checkcode/${codebar}/')}}`;
      var method = 'GET';

      $.ajax({
        url: url,
        method: method,
        success: function(codebar) {
          if (codebar === false) {
            $('.errorcode').html('');
            input.removeClass('hasError');
            input.addClass('hasSuccess');
            removeInputEmpty();
            newCode();
            proxInput();
            $(".CountCaracteresName").html('0/13 caracteres restantes');
            // console.log('Total de Digitos: ' + countLenght);
            // console.log('Código aceito, verifica se existe, sucesso criar o proximo e muda pra ele.');
          } else {
            input.addClass('hasError');
            // input.after('<span class="errorcode help-block text-danger">Código já existe ou é inválido</span>');
            // $('.errorcode').html('');
            // $('.errorcode').addClass('hasError text-danger clearfix').html('Código inválido ou já cadastrado');
          }
        },
        error: function(xhr) {
          console.log('preciso add a class de error e não criar o novo input')
          // console.log(xhr);
        }
      });

    }
  }


  function proxInput() {
    $('.codeInput').last().focus();
  }

  function newCode() {
    var i = parseInt($("#inputCodes input").last().attr('data-inputcode'));
    i = i + 1;
    var input = '';
    input += '<div class="d-flex row' + i + '"><div class="flex-grow-1 mr-2">';
    input += '<input type="tel" id="code' + i + '" name="code[]" data-inputcode="' + i + '" class="form-control mb-2 codeInput" placeholder="Código de Barras (13 dígitos)" value=""  oninput="validCode($(this))">';
    input += '</div><div class=""><a href="javascript:;" data-removerow="' + i + '" class="btn btn-xs btn-thema mt-1 remove-input-code hide" onClick="removeRow($(this));" data-toggle="tooltip" data-placement="right" title="Remover linha"><i class="fa fa-times"></i></a></div></div>';
    $("#inputCodes").append(input);
  }



  function removeInputEmpty() {
    var inputEmpty = $(".codeInput").filter(function() {
      return !this.value;
    }).get();

    if (inputEmpty.length) {
      $(inputEmpty).remove();
    }
  }

  function removeRow(alvo) {
    var rowRemove = '.row' + alvo.attr('data-removerow');
    $(rowRemove).remove();
    $('.tooltip').remove();
    // console.log(rowRemove);
  }

  // Button Save Forms - Create and Edit
  $(document).on('click', '#btn-salvar', function(e) {
    e.preventDefault();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': "{{csrf_token()}}"
      }
    });

    removeInputEmpty();

    let id = $(this).data('id');
    var type = $(this).data('type');

    if (type == 'edit') {
      if (id) {
        var url = `{{ url($url_action.'-update/${id}') }}`;
        var method = 'PUT';
      }
    } else {
      var url = "{{ url($url_action.'-store') }}";
      var method = 'POST';
    }
    var data = $('#form-request').serialize();
    $.ajax({
      url: url,
      data: data,
      method: method,
      success: function(data) {
        Swal.fire({
          text: data,
          icon: 'success',
          showClass: {
            popup: 'animate_animated animate_backInUp'
          },
          onClose: () => {
            // limpa campos para novo insert
            // $('#form-request')[0].reset();
            // Loading page listagem
            location.href = "{{url($url_action)}}";
          }
        });
      },
      error: function(xhr) {
        if (xhr.status === 422) {
          Swal.fire({
            text: 'Validação: ' + xhr.responseJSON,
            icon: 'warning',
            showClass: {
              popup: 'animate_animated animate_wobble'
            }
          });
        } else {
          Swal.fire({
            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
            icon: 'error',
            showClass: {
              popup: 'animate_animated animate_wobble'
            }
          });
        }
      }
    });
  });
</script>
@endsection