@extends('backend.base')
@if(isset($result->code))
@section('title', 'Resultado da Busca')
@else
@section('title', 'Resultado Não Encontrado!')
@endif

@section('breadcrumb')
<div class="page-heading">
  <h1>@yield('title')</h1>
  <ul class="breadcrumb">
    <li><a href="{{ route('backend.home') }}">Início</a></li>
    <li><a href="">@yield('title')</a></li>
  </ul>
</div>
@endsection

@section('content')
<section>
  <div class="card">
    <div class="card-body">
      @if(isset($result->code))
      <div class="col-xs-12 col-md-12">
        <h4>Código de Barras: {{ $result->code }}</h4>
        <ul class="list-unstyled pl-2">
          <li>N° de Atendimento <a href="{{ url('buscar?code='.$infoAttendance->number)}}">{{ $infoAttendance->number }}</a></li>
          <li>Data do Checkin: {{ \Carbon\Carbon::parse($result->checkin_date)->format('d/m/Y H:i') }}</li>
          <li>Tipo de Serviço: {{ $result->service }}</li>
        </ul>
      </div>

      <hr>
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover mb-0">
          <thead>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'code', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> Fase</th>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'checkin_date', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> Data da Alteração</th>
            <th>Status</th>
          </thead>
          <tbody>
            <form class="form" id="form-table">
              @if($result->checkout_status)
              <tr>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Fase:</span> Checkout</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Data Atualização:</span> {{ \Carbon\Carbon::parse($result->checkout_date)->format('d/m/Y - H:i') }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Status:</span> 
                  @if($result->checkout_status == 'Concluído')
                  <span class="badge badge-success"><i class="fa fa-check"></i> {{$result->checkout_status}}</span>
                  @else
                  <span class="badge badge-warning {{ $result->checkout_status == 'Não controlado' ? 'badge-dark' : '' }}"><i class="fa fa-minus"></i> {{$result->checkout_status}}</span>
                  @endif
                </td>
              </tr>
              @endif


              @if($result->finalizado_status)
              <tr>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Fase:</span> Finalizado</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Data Atualização:</span> {{ \Carbon\Carbon::parse($result->finalizado_date)->format('d/m/Y - H:i') }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Status:</span> 
                  @if($result->finalizado_status == 'Concluído')
                  <span class="badge badge-success"><i class="fa fa-check"></i> {{$result->finalizado_status}}</span>
                  @else
                  <span class="badge badge-warning {{ $result->finalizado_status == 'Não controlado' ? 'badge-dark' : '' }}"><i class="fa fa-minus"></i> {{$result->finalizado_status}}</span>
                  @endif
                </td>
              </tr>
              @endif

              @if($result->acabamento_status)
              <tr>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Fase:</span> Acabamento</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Data Atualização:</span> {{ \Carbon\Carbon::parse($result->acabamento_date)->format('d/m/Y - H:i') }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Status:</span> 
                  @if($result->acabamento_status == 'Concluído')
                  <span class="badge badge-success"><i class="fa fa-check"></i> {{$result->acabamento_status}}</span>
                  @else
                  <span class="badge badge-warning {{ $result->acabamento_status == 'Não controlado' ? 'badge-dark' : '' }}"><i class="fa fa-minus"></i> {{$result->acabamento_status}}</span>
                  @endif
                </td>
              </tr>
              @endif

              @if($result->secagem_status)
              <tr>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Fase:</span> Secagem</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Data Atualização:</span> {{ \Carbon\Carbon::parse($result->secagem_date)->format('d/m/Y - H:i') }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Status:</span> 
                  @if($result->secagem_status == 'Concluído')
                  <span class="badge badge-success"><i class="fa fa-check"></i> {{$result->secagem_status}}</span>
                  @else
                  <span class="badge badge-warning {{ $result->secagem_status == 'Não controlado' ? 'badge-dark' : '' }}"><i class="fa fa-minus"></i> {{$result->secagem_status}}</span>
                  @endif
                </td>
              </tr>
              @endif

              @if($result->lavagem_status)
              <tr>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Fase:</span> Lavagem</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Data Atualização:</span> {{ \Carbon\Carbon::parse($result->lavagem_date)->format('d/m/Y - H:i') }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Status:</span> 
                  @if($result->lavagem_status == 'Concluído')
                  <span class="badge badge-success"><i class="fa fa-check"></i> {{$result->lavagem_status}}</span>
                  @else
                  <span class="badge badge-warning {{ $result->lavagem_status == 'Não controlado' ? 'badge-dark' : '' }}"><i class="fa fa-minus"></i> {{$result->lavagem_status}}</span>
                  @endif
                </td>
              </tr>
              @endif

              @if($result->checkin_status)
              <tr>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Fase:</span> CheckIn</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Data Atualização:</span> {{ \Carbon\Carbon::parse($result->checkin_date)->format('d/m/Y - H:i') }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Status:</span> 
                  @if($result->checkin_status == 'Concluído')
                  <span class="badge badge-success"><i class="fa fa-check"></i> {{$result->checkin_status}}</span>
                  @else
                  <span class="badge badge-warning {{ $result->checkin_status == 'Não controlado' ? 'badge-dark' : '' }}"><i class="fa fa-minus"></i> {{$result->checkin_status}}</span>
                  @endif
                </td>
              </tr>
              @endif
            </form>
          </tbody>
        </table>
      </div>
      <a href="javascript:history.back()" class="btn btn-sm btn-thema mt-3"><i class="fa fa-arrow-left"></i> Voltar</a>
      @else
      <h5>Desculpe, mas não encontramos nenhum <b>Número de Atendimento</b> ou <b>Código de Barras</b>.</h5>
      <p>Revise o <b>número digitado</b> ou cadastre um novo código de barras</p>
      <a href="{{ url('/checkin-create') }}" class="btn btn-sm btn-thema mt-3">Novo Atendimento</a>
      @endif

    </div>
  </div>
</section>
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection