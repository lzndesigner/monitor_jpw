<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Customer;
use DB;

class CustomerController extends Controller
{
    
    protected $model;
    protected $request;
    protected $fields;
    protected $datarequest;
    
    public function __construct(Customer $customer, Request $request)
    {
        $this->middleware('auth');
        $this->model                =  $customer;
        $this->request              =  $request;

        $this->datarequest = [
            'titulo'               =>  'Clientes',
            'diretorio'            =>  'backend.customers',
            'url_action'               =>  'customers'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $order = $this->request->input('order') == 'asc' ? 'desc' : 'asc';
            $column_name = null;

            if ($this->request->input('column')) {
                $column = $this->request->input('column');
                $column_name = "$column $order";
            } else {
                $column_name = "id desc";
            }

            $field = $this->request->input('field') ? $this->request->input('field') : 'name';
            $operador = $this->request->input('operador') ? $this->request->input('operador') : 'like';
            $value = $this->request->input('value') ? $this->request->input('value') : '';

            if ($field == 'data' || $field == 'dataini' || $field == 'datafim') {
                $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
            }

            if ($operador == 'like') {
                $newValue = "'%$value%'";
            } else {
                $newValue = "'$value'";
            }

            $results = DB::table('customers')
                ->select('id', 'name', 'company', 'email', 'status', 'phone', 'created_at')
                ->orderByRaw("$column_name")
                ->whereraw("$field $operador $newValue")
                ->paginate(10);
        } catch (\Exception $err) {
            return response()->json($err->getMessage(), 500);
        }

        return view($this->datarequest['diretorio'] . '.index', compact('results', 'order'))->with($this->datarequest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->datarequest['diretorio'] . '.form')->with($this->datarequest);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $model = new $this->model;
        $result = $this->request->all();

        $rules = [
            'name'          => "required",
            'email'         => "required|unique:customers",
            'password'      => 'required|min:6|confirmed',
            'company'       => "required",
            'cep'           => "required",
            'address'       => "required",
            'number'        => "required",
            'city'          => "required",
            'state'         => "required",
            'phone'         => "required"
        ];

        $messages = [
            'name.required' => 'nome é obrigatório',
            'email.required' => 'e-mail é obrigatório',
            'password.required' => 'senha é obrigatório',
            'password.min' => 'senha precisa ter 6 caracteres',
            'company.required' => 'empresa é obrigatório',
            'cep.required' => 'CEP é obrigatório',
            'address.required' => 'endereço é obrigatório',
            'number.required' => 'número é obrigatório',
            'city.required' => 'cidade é obrigatório',
            'state.required' => 'estado é obrigatório',
            'phone.required' => 'telefone é obrigatório'
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->company = $result['company'];
        $model->email = $result['email'];
        $model->password = $result['password'];
        $model->status = $result['status'];
        $model->cep = $result['cep'];
        $model->address = $result['address'];
        $model->number = $result['number'];
        $model->complement = $result['complement'];
        $model->city = $result['city'];
        $model->state = $result['state'];
        $model->phone = $result['phone'];
        $model->payment_method = $result['payment_method'];
        $model->balance_account = $result['balance_account'];
        $model->created_at = Carbon::now();
        $model->updated_at = Carbon::now();

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Registro salvo com sucesso', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = $this->model::find($id);
        $allCustomers = $this->model::where('id', '!=', $id)->get();

        return view($this->datarequest['diretorio'] . '.details', compact('result', 'allCustomers'))->with($this->datarequest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = $this->model::where('id', $id)->first();
        return view($this->datarequest['diretorio'] . '.form', compact('result'))->with($this->datarequest);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $model = $this->model::find($id);
        $result = $this->request->all();

        $rules = [
            'name'          => "required",
            'email'         => "required|unique:customers,email,$id,id",
            'password'      => 'nullable|min:6',
            'company'       => "required",
            'cep'           => "required",
            'address'       => "required",
            'number'        => "required",
            'city'          => "required",
            'state'         => "required",
            'phone'         => "required"
        ];

        $messages = [
            'name.required' => 'nome é obrigatório',
            'email.required' => 'e-mail é obrigatório',
            'company.required' => 'empresa é obrigatório',
            'cep.required' => 'CEP é obrigatório',
            'address.required' => 'endereço é obrigatório',
            'number.required' => 'número é obrigatório',
            'city.required' => 'cidade é obrigatório',
            'state.required' => 'estado é obrigatório',
            'phone.required' => 'telefone é obrigatório'
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->company = $result['company'];
        $model->email = $result['email'];
        if ($result['password']) {
            $password = bcrypt($result['password']);
            $model->password = $password;
        }
        if (isset($result['status'])) {
            $model->status = $result['status'];
        } else {
            $model->status = 'Pendente';
        }
        $model->cep = $result['cep'];
        $model->address = $result['address'];
        $model->number = $result['number'];
        $model->complement = $result['complement'];
        $model->city = $result['city'];
        $model->state = $result['state'];
        $model->phone = $result['phone'];
        $model->payment_method = $result['payment_method'];
        $model->balance_account = $result['balance_account'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Registro salvo com sucesso', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $model = new $this->model;
        $data = $this->request->all();

        if (!isset($data['selected'])) {
            return response()->json('Selecione ao menos um registro', 422);
        }

        try {
            foreach ($data['selected'] as $result) {
                $find = $model->find($result);
                $find->delete();
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }
}
