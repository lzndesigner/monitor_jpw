<form action="" class="form-horizontal" id="form-request">
  <fieldset>
    <div class="d-column d-sm-column d-md-flex align-items-center">
      <div class="mr-3">
        <div class="form-group">
          <label for="checkin_date" class="col-form-label">Data Checkin <small>(<span class="text-danger">*</span>)</small>:</label>
          <input type="text" id="checkin_date" name="checkin_date" class="form-control" placeholder="Data Checkin" value="{{isset($result->checkin_date) ? $result->checkin_date : \Carbon\Carbon::now()->format('d/m/Y H:i')  }}" required>
        </div><!-- form-group -->
      </div><!--  -->

      <div class="mr-3">
        <div class="form-group">
          <label for="service" class="col-form-label">Serviço <small>(<span class="text-danger">*</span>)</small>:</label>
          <select name="service" id="service" class="form-control">
            <option value="Tapete">Tapete</option>
            <option value="Persiana">Persiana</option>
            <option value="Carpete">Carpete</option>
            <option value="Edredon">Edredon</option>
            <option value="Cortina">Cortina</option>
            <option value="Futton">Futton</option>
            <option value="Almofada">Almofada</option>
          </select>
        </div><!-- form-group -->
      </div><!--  -->

      <div class="">
        <div class="form-group">
          <label for="number" class="col-form-label">N° Atendimento <small>(<span class="text-danger">*</span>)</small>:</label>
          <input type="tel" id="number" name="number" class="form-control" placeholder="0001" value="{{isset($result->number) ? $result->number : ''}}" required>
        </div><!-- form-group -->
      </div><!--  -->


    </div><!-- form-row -->
  </fieldset>

  <fieldset>
    <div class="form-row">
      <div class="col-xs-12 col-md-6">
        <div class="form-group">
          <label for="code" class="col-form-label">Códigos de barras (<span class="text-danger">*</span>):</label>
          <div id="inputCodes">
            <div class="d-flex row1">
              <div class="flex-grow-1 mr-2">
                <input type="tel" id="code1" name="code[]" data-inputcode="1" class="form-control mb-2 codeInput" placeholder="Código de Barras (13 dígitos)" value="" required oninput="validCode($(this));">
              </div>
              <div class="">
                <a href="javascript:;" data-removerow="1" class="btn btn-xs btn-thema mt-1 remove-input-code hide" onClick="removeRow($(this));" data-toggle="tooltip" data-placement="right" title="Remover linha"><i class="fa fa-times"></i></a>
              </div>
            </div>
          </div>
          <span class="errorcode"></span>
          <span class="CountCaracteresName"></span>
        </div><!-- form-group -->
      </div><!-- col -->
    </div><!-- form-row -->
  </fieldset>

  <div class="modal-footer justify-content-start">
    <button type="submit" class="btn btn-sm btn-success" id="btn-salvar" data-type="create"><i class="fa fa-check"></i> Salvar</button>
    <a href="{{ route('backend.steps.checkin') }}" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Cancelar</a>
  </div>
</form>