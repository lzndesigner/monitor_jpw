<?php

use Illuminate\Database\Seeder;

class AttendanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attendances')->insert([
            'number' => '1',
            'date_checkin' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        
        DB::table('attendance_steps')->insert([
          'number_id' => '1',
          'fase' => 'checkin',
          'code' => '1234567890123',
          'service' => 'tapete',
          'status' => 'em andamento',
          'date_update' => \Carbon\Carbon::now(),
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now(),
      ]);

    }
}
