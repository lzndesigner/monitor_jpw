<form action="" class="form-horizontal" id="form-request">
  <fieldset>
    <div class="form-row">
      <div class="col-xs-12 col-md-6">
        <div class="form-group">
          <label for="code" class="col-form-label">Códigos de barras (<span class="text-danger">*</span>):</label>
          <div id="inputCodes">
            <div class="d-flex row1">
              <div class="flex-grow-1 mr-2">
                <input type="tel" id="code1" name="code[]" data-inputcode="1" class="form-control mb-2 codeInput" placeholder="Código de Barras (13 dígitos)" value="" required oninput="validCode($(this));">
              </div>
              <div class="">
                <a href="javascript:;" data-removerow="1" class="btn btn-xs btn-thema mt-1 remove-input-code hide" onClick="removeRow($(this));" data-toggle="tooltip" data-placement="right" title="Remover linha"><i class="fa fa-times"></i></a>
              </div>
            </div>
          </div>
          <div class="errocode"></div>
          <div class="CountCaracteresName"></div>
        </div><!-- form-group -->
      </div><!-- col -->
    </div><!-- form-row -->
  </fieldset>

  <div class="modal-footer justify-content-start">
    <button type="submit" class="btn btn-sm btn-success" id="btn-salvar" data-type="create"><i class="fa fa-check"></i> Salvar</button>
    <a href="{{ route('backend.steps.acabamento') }}" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Cancelar</a>
  </div>
</form>