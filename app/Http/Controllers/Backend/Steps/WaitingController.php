<?php

namespace App\Http\Controllers\Backend\Steps;

use App\Http\Controllers\Controller;
use App\Models\AttendanceSteps;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WaitingController extends Controller
{

    protected $model;
    protected $request;
    protected $fields;
    protected $datarequest;

    public function __construct(AttendanceSteps $attendancestep, Request $request)
    {
        $this->middleware('auth');
        $this->model = $attendancestep;
        $this->request = $request;

        $this->datarequest = [
            'titulo' => 'Fase: Aguardando Retirada',
            'diretorio' => 'backend.steps.waiting',
            'url_action' => 'waiting',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $order = $this->request->input('order') == 'asc' ? 'desc' : 'asc';
            $column_name = null;

            if ($this->request->input('column')) {
                $column = $this->request->input('column');
                $column_name = "$column $order";
            } else {
                $column_name = "checkin_date desc";
            }

            $field = $this->request->input('field') ? $this->request->input('field') : 'number_id';
            $operador = $this->request->input('operador') ? $this->request->input('operador') : 'like';
            $value = $this->request->input('value') ? $this->request->input('value') : '';

            if ($field == 'data' || $field == 'dataini' || $field == 'datafim') {
                $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
            }

            if ($operador == 'like') {
                $newValue = "'%$value%'";
            } else {
                $newValue = "'$value'";
            }

            $results = DB::table('attendance_steps')
                ->select('id', 'number_id', 'code', 'service', 'checkin_date', 'checkout_date', 'checkout_status', 'updated_at')
                ->orderByRaw("$column_name")
                ->whereraw("$field $operador $newValue")
                ->where('checkout_status', 'Aguardando')
                ->get();
        } catch (\Exception $err) {
            return response()->json($err->getMessage(), 500);
        }

        return view($this->datarequest['diretorio'] . '.index', compact('results', 'order'))->with($this->datarequest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->datarequest['diretorio'] . '.create')->with($this->datarequest);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $model = new $this->model;
        $result = $this->request->all();

        $rules = [
            'code' => "required|array",
        ];

        $messages = [
            'code.required' => 'código de barras é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }
        foreach ($result as $codes) {
            foreach ($codes as $code) {
                $row = AttendanceSteps::findByCode($code);

                if (!$row) {
                    continue;
                }

                $row->checkin_status = AttendanceSteps::CONCLUIDO;
                $row->lavagem_date = !$row->lavagem_status ? now() : $row->lavagem_date;
                $row->lavagem_status = !$row->lavagem_status ? AttendanceSteps::NAO_CONTROLADO : AttendanceSteps::CONCLUIDO;
                $row->secagem_date = !$row->secagem_status ? now() : $row->secagem_date;
                $row->secagem_status = !$row->secagem_status ? AttendanceSteps::NAO_CONTROLADO : AttendanceSteps::CONCLUIDO;
                $row->acabamento_date = !$row->acabamento_status ? now() : $row->acabamento_date;
                $row->acabamento_status = !$row->acabamento_status ? AttendanceSteps::NAO_CONTROLADO : AttendanceSteps::CONCLUIDO;
                $row->finalizado_date = !$row->finalizado_status ? now() : $row->finalizado_date;
                $row->finalizado_status = !$row->finalizado_status ? AttendanceSteps::NAO_CONTROLADO : AttendanceSteps::CONCLUIDO;
                $row->checkout_date = now();
                $row->checkout_status = AttendanceSteps::AGUARDANDO;
                $row->save();
            }
        }

        return response()->json('Registros Atualizados para Aguardando Retirada!', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = $this->model::find($id);
        $allCustomers = $this->model::where('id', '!=', $id)->get();

        return view($this->datarequest['diretorio'] . '.details', compact('result', 'allCustomers'))->with($this->datarequest);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $model = new $this->model;
        $data = $this->request->all();

        if (!isset($data['selected'])) {
            return response()->json('Selecione ao menos um registro', 422);
        }

        try {
            foreach ($data['selected'] as $result) {
                $find = $model->find($result);
                $find->finalizado_status = 'Em andamento';
                $find->checkout_date = null;
                $find->checkout_status = null;
                $find->save();
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }
}
