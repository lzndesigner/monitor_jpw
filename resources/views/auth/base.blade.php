<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Lavanderia Alves & Stylo Lavanderia</title>

  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
  <!-- Include Fontawesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{ asset('general/css/bootstrap.css?').rand() }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('backend/css/login.css?').rand() }}" type="text/css" />

</head>

<body class="text-center">
  <div id="app" class="form-signin">
    <div class="logo mb-3">
      <img src="{{ asset('general/images/logo-black.png') }}" alt="" class="img-fluid mx-2" style="width:130px;">
      <img src="{{ asset('general/images/logo_stylo.png') }}" alt="" class="img-fluid mx-2" style="width:130px;">
    </div>
    <main class="py-4">
      @yield('content')
    </main>
  </div><!-- #app -->
</body>

</html>