<?php

namespace App\Observers;

use App\Events\NewAttendanceEvent;
use App\Events\RemoveAttendanceEvent;
use App\Events\UpdateAttendanceEvent;
use App\Models\AttendanceSteps;
use Throwable;

class AttendanceStepObserve
{
    public function created(AttendanceSteps $model)
    {
        try {
            logger('send broadcast: add');
            broadcast(new NewAttendanceEvent($model));
        } catch (Throwable $e) {
            logger()->warning($e->getMessage());
        }
    }

    public function updated(AttendanceSteps $model)
    {
        try {
            logger('send broadcast: update');
            broadcast(new UpdateAttendanceEvent($model));
        } catch (Throwable $e) {
            logger()->warning($e->getMessage());
        }
    }

    public function deleted(AttendanceSteps $model)
    {
        try {
            logger('send broadcast: remove');
            broadcast(new RemoveAttendanceEvent($model));
        } catch (Throwable $e) {
            logger()->warning($e->getMessage());
        }
    }
}
