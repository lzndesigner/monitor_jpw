<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property AttendanceSteps|HasOne $step
 */
class Attendance extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number',
        'date_checkin',
    ];

    public function step()
    {
        return $this->hasOne(AttendanceSteps::class, 'number_id', 'number');
    }

    public static function findByNumber($number)
    {
        return self::query()->where('number', $number)->first();
    }

}
