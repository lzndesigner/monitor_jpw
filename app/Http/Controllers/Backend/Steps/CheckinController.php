<?php

namespace App\Http\Controllers\Backend\Steps;

use App\Http\Controllers\Controller;
use App\Models\Attendance;
use App\Models\AttendanceSteps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CheckinController extends Controller
{

    protected $model;
    protected $request;
    protected $fields;
    protected $datarequest;

    public function __construct(AttendanceSteps $attendancestep, Request $request)
    {
        $this->middleware('auth');
        $this->model = $attendancestep;
        $this->request = $request;

        $this->datarequest = [
            'titulo' => 'Fase: Check-In',
            'diretorio' => 'backend.steps.checkin',
            'url_action' => 'checkin',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            list($order, $results) = AttendanceSteps::filter($request, AttendanceSteps::STEP_CHECKIN);
        } catch (\Exception $err) {
            return response()->json($err->getMessage(), 500);
        }

        return view($this->datarequest['diretorio'] . '.index', compact('results', 'order'))->with($this->datarequest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->datarequest['diretorio'] . '.create')->with($this->datarequest);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $result = $this->request->all();

        $rules = [
            'number' => "required",
            'code' => "required|array",
        ];

        $messages = [
            'number.required' => 'número de atendimento é obrigatório',
            'code.required' => 'código de barras é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $attendance = Attendance::firstOrNew(['number' => $result['number']]);
        $attendance->date_checkin = now();
        $attendance->save();

        foreach ($result['code'] as $codes) {
            AttendanceSteps::create([
                'number_id' => $result['number'],
                'code' => $codes,
                'service' => $result['service'],
                'checkin_date' => now(),
                'checkin_status' => 'Em Andamento',
            ]);
        }

        return response()->json('Registro salvo com sucesso', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = $this->model::find($id);
        $allCustomers = $this->model::where('id', '!=', $id)->get();

        return view($this->datarequest['diretorio'] . '.details', compact('result', 'allCustomers'))->with($this->datarequest);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $data = $this->request->all();

        if (!isset($data['selected'])) {
            return response()->json('Selecione ao menos um registro', 422);
        }

        try {
            $rows = AttendanceSteps::query()
                ->whereIn('id', $data['selected'])
                ->delete();
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json($rows, 200);
    }
}
