<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Attendance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{

    protected $model;
    protected $request;
    protected $fields;
    protected $datarequest;

    public function __construct(Attendance $attendance, Request $request)
    {
        $this->middleware('auth');
        $this->model = $attendance;
        $this->request = $request;

        $this->datarequest = [
            'titulo' => 'Atendimentos',
            'diretorio' => 'backend.attendances',
            'url_action' => 'atendimentos',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $order = $this->request->input('order') == 'asc' ? 'desc' : 'asc';
            $column_name = null;

            if ($this->request->input('column')) {
                $column = $this->request->input('column');
                $column_name = "$column $order";
            } else {
                $column_name = "id desc";
            }

            $field = $this->request->input('field') ? $this->request->input('field') : 'number';
            $operador = $this->request->input('operador') ? $this->request->input('operador') : 'like';
            $value = $this->request->input('value') ?? '';

            if ($field == 'data' || $field == 'dataini' || $field == 'datafim') {
                $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
            }

            $newValue = $operador == 'like' ? "'%{$value}%'" : "'$value'";

            $results = Attendance::query()
                ->select('attendances.id as id', 'attendances.number', 'attendances.date_checkin')
                ->addSelect(DB::raw("COUNT(attendance_steps.number_id) as qtaCodes"))
                ->leftjoin("attendance_steps", "attendance_steps.number_id", "attendances.number")
                ->groupBy('attendances.id', 'attendances.number', 'attendances.date_checkin')
                ->orderByRaw("$column_name")
                ->whereraw("$field $operador $newValue")
                ->paginate();

        } catch (\Exception $err) {
            return response()->json($err->getMessage(), 500);
        }

        return view($this->datarequest['diretorio'] . '.index', compact('results', 'order'))->with($this->datarequest);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroyAttendance()
    {
        $data = $this->request->all();

        if (!isset($data['selected'])) {
            return response()->json('Selecione ao menos um registro', 422);
        }

        try {
            DB::transaction(function () use ($data) {
                $scope = Attendance::query()
                    ->whereIn('number', $data['selected']);

                $scope->get()->each->step->delete();

                $scope->delete();
            });
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }
        return response()->json($this->request->all(), 200);
    }
}
