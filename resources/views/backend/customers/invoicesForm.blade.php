<p><small><b>Campos obrigatórios (<span class="text-danger">*</span>)</b></small></p>

<input type="hidden" id="customer_id" name="customer_id" value="{{isset($customer_id) ? $customer_id : 0}}">

<fieldset>
  <h6>Informações da Fatura</h6>
  <div class="form-row align-items-center">
    <div class="col-xs-7 col-md-7">
      <div class="form-group">
        <label for="description" class="col-form-label">Descrição (<span class="text-danger">*</span>):</label>
        <input type="text" id="description" name="description" class="form-control" placeholder="Descrição" value="{{isset($result->description) ? $result->description : ''}}" required>
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-3 col-md-3">
      <div class="form-group">
        <label for="price" class="col-form-label">Valor (<span class="text-danger">*</span>):</label>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">R$</span>
          </div>
          <input type="text" id="price" name="price" class="form-control formatedPrice" placeholder="100,00" value="{{isset($result->price) ? $result->price : ''}}" required>
        </div>
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-2 col-md-2">
      <div class="form-group">
        <label for="status" class="col-form-label">Status:</label>
        <select name="status" id="status" class="form-control">
          @if(isset($result->status))
          <option value="pago" {{ $result->status == "pago" ? 'selected' : ''}}>Pago</option>
          <option value="nao_pago" {{ $result->status == "nao_pago" ? 'selected' : ''}}>Não pago</option>
          <option value="cancelado" {{ $result->status == "cancelado" ? 'selected' : ''}}>Cancelado</option>
          @else
          <option value="pago">Pago</option>
          <option value="nao_pago">Não pago</option>
          <option value="cancelado">Cancelado</option>
          @endif
        </select>
      </div><!-- form-group -->
    </div><!-- col -->
  </div><!-- form-row -->

  <div class="form-row">
    <div class="col-xs-3 col-md-3">
      <div class="form-group">
        <label for="date_invoice" class="col-form-label">Data Fatura (<span class="text-danger">*</span>):</label>
        <input type="text" id="date_invoice" name="date_invoice" class="form-control" placeholder="Data Fatura" value="{{isset($result->date_invoice) ? $result->date_invoice : \Carbon\Carbon::now()->format('Y-m-d') }}" required>
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-3 col-md-3">
      <div class="form-group">
        <label for="date_end" class="col-form-label">Data Vencimento (<span class="text-danger">*</span>):</label>
        <input type="text" id="date_end" name="date_end" class="form-control" placeholder="Data Vencimento" value="{{isset($result->date_end) ? $result->date_end : ''}}" required>
      </div><!-- form-group -->
    </div><!-- col -->

    <div class="col-xs-3 col-md-3">
      <div class="form-group">
        <label for="date_payment" class="col-form-label">Data Pagamento:</label>
        <input type="text" id="date_payment" name="date_payment" class="form-control" placeholder="Data Pagamento" value="{{isset($result->date_payment) ? $result->date_payment : ''}}">
      </div><!-- form-group -->
    </div><!-- col -->


    <div class="col-xs-3 col-md-3">
      <div class="form-group">
        <label for="payment_method" class="col-form-label">Método:</label>
        <select name="payment_method" id="payment_method" class="form-control">
          <option value="mercadopago" selected>Mercado Pago</option>
          <option value="bancodobrasil">Banco do Brasil</option>
          <option value="bancodobradesco">Banco do Bradesco</option>
          <option value="bancodocaixa">Banco do Caixa</option>
        </select>
      </div><!-- form-group -->
    </div><!-- col -->
  </div><!-- form-row -->
</fieldset>

<fieldset>
  <div class="form-row">
    <div class="col-xs-12 col-md-6">
      <div class="card">
        <div class="card-body">
          <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="sendmailinvoice">
            <label class="form-check-label" for="sendmailinvoice">Marque para Enviar Email de Confirmação</label>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Confirmar Pagamento</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</fieldset>