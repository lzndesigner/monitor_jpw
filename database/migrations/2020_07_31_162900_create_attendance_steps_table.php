<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendanceStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_steps', function (Blueprint $table) {
            $table->id('id');
            $table->string('number_id');
            $table->string('code');
            $table->string('service');
            $table->dateTime('checkin_date')->nullable();
            $table->string('checkin_status')->nullable();
            $table->dateTime('lavagem_date')->nullable();
            $table->string('lavagem_status')->nullable();
            $table->dateTime('secagem_date')->nullable();
            $table->string('secagem_status')->nullable();
            $table->dateTime('acabamento_date')->nullable();
            $table->string('acabamento_status')->nullable();
            $table->dateTime('finalizado_date')->nullable();
            $table->string('finalizado_status')->nullable();
            $table->dateTime('checkout_date')->nullable();
            $table->string('checkout_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_steps');
    }
}
