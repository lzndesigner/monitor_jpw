<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use LogicException;

/**
 * @property Carbon $creatd_at
 * @property Carbon $updated_at
 */
class AttendanceSteps extends Model
{
    const STEP_CHECKIN = 'checkin';

    const STEP_LAVAGEM = 'lavagem';

    const STEP_SECAGEM = 'secagem';

    const STEP_ACABAMENTO = 'acabamento';

    const STEP_FINALIZADO = 'finalizado';

    const STEP_CHECKOUT = 'checkout';

    const STEPS = [
        self::STEP_CHECKIN,
        self::STEP_LAVAGEM,
        self::STEP_SECAGEM,
        self::STEP_ACABAMENTO,
        self::STEP_FINALIZADO,
        self::STEP_CHECKOUT,
    ];

    const EM_ANDAMENTO = 'Em Andamento';

    const CONCLUIDO = 'Concluído';

    const NAO_CONTROLADO = 'Não Controlado';

    const AGUARDANDO = 'Aguardando';

    const STATUS = [
        self::EM_ANDAMENTO,
        self::CONCLUIDO,
        self::NAO_CONTROLADO,
        self::AGUARDANDO,
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attendance_steps';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number_id',
        'code',
        'service',
        'checkin_date',
        'checkin_status',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'next_step',
        'prev_step',
        'current_step',
        'future',
        'steps',
    ];

    public function getStepsAttribute()
    {
        $steps = [];

        foreach (self::STEPS as $step) {
            $status = "{$step}_status";
            $date = "{$step}_date";

            if (!$this->{$status} && !$this->{$date}) {
                $steps[$step] = 'not_initialized';
                continue;
            }

            if ($this->{$status} === self::EM_ANDAMENTO && $this->updated_at->eq($this->{$date})) {
                $steps[$step] = 'in_process';
                continue;
            }

            $steps[$step] = 'ok';
        }

        return collect($steps);
    }

    public function getNextStepAttribute()
    {
        return $this->future->first();
    }

    public function getPrevStepAttribute()
    {
        return $this->steps
            ->filter(function ($step) {
                return $step === 'ok';
            })
            ->keys()
            ->last();
    }

    public function getFutureAttribute()
    {
        return $this->steps
            ->filter(function ($step) {
                return $step === 'not_initialized';
            })
            ->keys();
    }

    public function getCurrentStepAttribute()
    {
        return $this->steps
            ->filter(function ($step) {
                return $step === 'in_process';
            })
            ->keys()
            ->first();
    }

    public function scopeFilter(Builder $query, Request $request, string $step)
    {
        throw_if(!in_array($step, self::STEPS), LogicException::class);

        $order = $request->input('order') == 'asc' ? 'desc' : 'asc';
        $column_name = null;

        if ($request->input('column')) {
            $column = $request->input('column');
            $column_name = "$column $order";
        } else {
            $column_name = "checkin_date desc";
        }

        $field = $request->input('field') ? $request->input('field') : 'number_id';
        $operador = $request->input('operador') ? $request->input('operador') : 'like';
        $value = $request->input('value') ? $request->input('value') : '';

        if ($field == 'data' || $field == 'dataini' || $field == 'datafim') {
            $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }

        if ($operador == 'like') {
            $newValue = "'%$value%'";
        } else {
            $newValue = "'$value'";
        }

        return [
            $order,
            $query
                ->whereraw("$field $operador $newValue")
                ->where("{$step}_status", $step != 'checkout' ? self::EM_ANDAMENTO : self::CONCLUIDO)
                ->orderByRaw("$column_name")
                ->paginate(
                    $request->input('paginate', 15)
                ),
        ];
    }

    public static function findByCode($code)
    {
        return self::query()->firstWhere('code', '=', $code);
    }
}
