@extends('backend.base')
@if($infoAttendance)
@section('title', 'Resultado da Busca')
@else
@section('title', 'Resultado Não Encontrado!')
@endif

@section('breadcrumb')
<div class="page-heading">
  <h1>@yield('title')</h1>
  <ul class="breadcrumb">
    <li><a href="{{ route('backend.home') }}">Início</a></li>
    <li><a href="">@yield('title')</a></li>
  </ul>
</div>
@endsection

@section('content')
<section>
  <div class="card">
    <div class="card-body">
      @if($infoAttendance)
      <div class="col-xs-12 col-md-5">
        <h4>N° de Atendimento {{ $infoAttendance->number }}</h4>
        <ul class="list-unstyled pl-2">
          <li>Data do Checkin: {{ \Carbon\Carbon::parse($infoAttendance->date_checkin)->format('d/m/Y H:i') }}</li>
        </ul>
      </div>

      <hr>
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover mb-0">
          <thead>
            <th width="40px">
              <div class="form-group form-check">
                <input type="checkbox" id="checkAll" value="" class="form-check-input">
              </div>
            </th>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'code', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> Código de Barras</th>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'service', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> Serviço</th>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'checkin_date', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> Data Checkin</th>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'updated_at', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> Última Alteração</th>
            <th>Fase</th>
          </thead>
          <tbody>
            @php
            $datenow = \Carbon\Carbon::now();
            $dateSevendays = \Carbon\Carbon::now()->subDays(7);
            $dateNinedays = \Carbon\Carbon::now()->subDays(9);
            $dateTendays = \Carbon\Carbon::now()->subDays(10);
            @endphp
            <form class="form" id="form-table">
              {{ csrf_field() }}
              @foreach($codes as $code)
              @php
              $dateLastUpdate = \Carbon\Carbon::parse($code->updated_at);
              @endphp
              @if($dateLastUpdate < $dateSevendays) @php($classTable='table-warning' ) @endif @if($dateLastUpdate < $dateNinedays) @php($classTable='table-danger' ) @endif @if($dateLastUpdate < $dateTendays) @php($classTable='table-dark' ) @endif
              
              <tr class="{{ $code->checkout_status != 'Concluído' ? isset($classTable) ? $classTable : '' : '' }}">
                <td>
                  <div class="form-group form-check">
                    <input type="checkbox" name="selected[]" value="{{ $code->id }}" class="form-check-input">
                  </div>
                </td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Código de Barras:</span> <a href="{{ url('buscar?code='.$code->code)}}">{{ $code->code }}</a></td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Serviço:</span> {{ $code->service }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Data Checkin:</span> {{ \Carbon\Carbon::parse($code->checkin_date)->format('d/m/Y - H:i') }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Última Alteração:</span> {{ \Carbon\Carbon::parse($code->updated_at)->format('d/m/Y - H:i') }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Status:</span>
                  @if($code->checkout_status == 'Concluído')
                  <span class="badge badge-success"><i class="fa fa-check"></i> Checkout</span>
                  @elseif($code->finalizado_status == 'Em andamento')
                  <span class="badge badge-warning"><i class="fa fa-minus"></i> Finalizado</span>
                  @elseif($code->acabamento_status == 'Em andamento')
                  <span class="badge badge-warning"><i class="fa fa-minus"></i> Acabamento</span>
                  @elseif($code->secagem_status == 'Em andamento')
                  <span class="badge badge-warning"><i class="fa fa-minus"></i> Secagem</span>
                  @elseif($code->lavagem_status == 'Em andamento')
                  <span class="badge badge-warning"><i class="fa fa-minus"></i> Lavagem</span>
                  @else
                  <span class="badge badge-warning"><i class="fa fa-minus"></i> Checkin</span>
                  @endif
                </td>
                </tr>
                @endforeach
            </form>
          </tbody>
        </table>
      </div>
      <div class="d-flex align-items-center mt-3">
        <div class="mr-2"><small>Com os Selecionados:</small></div>
        <div><a href="javascript:;" id="btn-delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i> Remover selecionados</a></div>
        <div class="flex-grow-1">
          {{$codes->appends(request()->query())->links() }}
        </div>
      </div><!-- d-flex -->
      @else
      <h5>Desculpe, mas não encontramos nenhum <b>Número de Atendimento</b> ou <b>Código de Barras</b>.</h5>
      <p>Revise o <b>número digitado</b> ou cadastre um novo código de barras</p>
      <a href="{{ url('/checkin-create') }}" class="btn btn-sm btn-thema mt-3">Novo Atendimento</a>
      @endif
    </div>
  </div>
</section>
@endsection

@section('cssPage')
<link rel="stylesheet" href="/general/plugins/sweetalert/sweetalert2.min.css">
@endsection

@section('jsPage')
<script src="/general/plugins/sweetalert/sweetalert2.min.js"></script>
<script>
  $("#checkAll").click(function() {
    $('input:checkbox').not(this).prop('checked', this.checked);
  });

  // Button - Delete - Not action Waiting
  $('#btn-delete').click(function(e) {
    Swal.fire({
      title: 'Deseja remover este registro?',
      text: "Você não poderá reverter isso!",
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, deletar!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: "{{url('codebar-delete')}}",
          method: 'POST',
          data: $('#form-table').serialize(),
          success: function(data) {
            // Loading page listagem
            location.reload();
          },
          error: function(xhr) {
            if (xhr.status === 422) {
              Swal.fire({
                text: xhr.responseJSON,
                icon: 'warning',
                showClass: {
                  popup: 'animate_animated animate_wobble'
                }
              });
            } else {
              Swal.fire({
                text: xhr.responseJSON,
                icon: 'error',
                showClass: {
                  popup: 'animate_animated animate_wobble'
                }
              });
            }
          }
        });
      }
    });

  });
</script>
@endsection