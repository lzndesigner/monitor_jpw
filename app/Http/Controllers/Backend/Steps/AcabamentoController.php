<?php

namespace App\Http\Controllers\Backend\Steps;

use App\Http\Controllers\Controller;
use App\Models\AttendanceSteps;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AcabamentoController extends Controller
{

    protected $model;
    protected $request;
    protected $fields;
    protected $datarequest;

    public function __construct(AttendanceSteps $attendancestep, Request $request)
    {
        $this->middleware('auth');
        $this->model = $attendancestep;
        $this->request = $request;

        $this->datarequest = [
            'titulo' => 'Fase: Acabamento',
            'diretorio' => 'backend.steps.acabamento',
            'url_action' => 'acabamento',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            list($order, $results) = AttendanceSteps::filter($request, AttendanceSteps::STEP_ACABAMENTO);
        } catch (\Exception $err) {
            return response()->json($err->getMessage(), 500);
        }

        return view($this->datarequest['diretorio'] . '.index', compact('results', 'order'))->with($this->datarequest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->datarequest['diretorio'] . '.create')->with($this->datarequest);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $model = new $this->model;
        $result = $this->request->all();

        $rules = [
            'code' => "required|array",
        ];

        $messages = [
            'code.required' => 'código de barras é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }
        foreach ($result as $codes) {
            foreach ($codes as $code) {

                $row = AttendanceSteps::findByCode($code);

                if (!$row) {
                    continue;
                }

                $row->checkin_status = AttendanceSteps::CONCLUIDO;
                $row->lavagem_date = !$row->lavagem_status ? now() : $row->lavagem_date;
                $row->lavagem_status = !$row->lavagem_status ? AttendanceSteps::NAO_CONTROLADO : AttendanceSteps::CONCLUIDO;
                $row->secagem_date = !$row->secagem_status ? now() : $row->secagem_date;
                $row->secagem_status = !$row->secagem_status ? AttendanceSteps::NAO_CONTROLADO : AttendanceSteps::CONCLUIDO;
                $row->acabamento_date = now();
                $row->acabamento_status = AttendanceSteps::EM_ANDAMENTO;
                $row->save();
            }
        }

        return response()->json('Registros Atualizados para Acabamento!', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = $this->model::find($id);
        $allCustomers = $this->model::where('id', '!=', $id)->get();

        return view($this->datarequest['diretorio'] . '.details', compact('result', 'allCustomers'))->with($this->datarequest);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $model = new $this->model;
        $data = $this->request->all();

        if (!isset($data['selected'])) {
            return response()->json('Selecione ao menos um registro', 422);
        }

        try {
            foreach ($data['selected'] as $result) {
                $find = $model->find($result);
                $find->secagem_status = 'Em andamento';
                $find->acabamento_date = null;
                $find->acabamento_status = null;
                $find->save();
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }
}
