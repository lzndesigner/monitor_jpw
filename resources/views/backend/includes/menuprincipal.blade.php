<ul>
  <li class="@if(collect(request()->segments())->last() == '') active @endif"><a href="{{ route('backend.home') }}">Página Inicial</a></li>
  <li class="@if(collect(request()->segments())->last() == 'atendimentos' || collect(request()->segments()) == '["buscar"]') active @endif"><a href="{{ route('backend.atendimentos') }}">Atendimentos</a></li>
  <li class="@if(collect(request()->segments())->last() == 'checkin' || collect(request()->segments())->last() == 'checkin-create') active @endif"><a href="{{ route('backend.steps.checkin') }}">Check-In</a></li>
  <li class="@if(collect(request()->segments())->last() == 'lavagem' || collect(request()->segments())->last() == 'lavagem-create') active @endif"><a href="{{ route('backend.steps.lavagem') }}">Lavagem</a></li>
  <li class="@if(collect(request()->segments())->last() == 'secagem' || collect(request()->segments())->last() == 'secagem-create') active @endif"><a href="{{ route('backend.steps.secagem') }}">Secagem</a></li>
  <li class="@if(collect(request()->segments())->last() == 'acabamento' || collect(request()->segments())->last() == 'acabamento-create') active @endif"><a href="{{ route('backend.steps.acabamento') }}">Acabamento</a></li>
  <li class="@if(collect(request()->segments())->last() == 'finalizado' || collect(request()->segments())->last() == 'finalizado-create') active @endif"><a href="{{ route('backend.steps.finalizado') }}">Finalizado</a></li>
  <li class="@if(collect(request()->segments())->last() == 'checkout' || collect(request()->segments())->last() == 'checkout-create') active @endif"><a href="{{ route('backend.steps.checkout') }}">Check-Out</a></li>
  <li class="@if(collect(request()->segments())->last() == 'waiting' || collect(request()->segments())->last() == 'waiting-create') active @endif"><a href="{{ route('backend.steps.waiting') }}">Aguardando Retirada</a></li>
  <li class="@if(collect(request()->segments())->last() == 'monitoracao') active @endif"><a href="{{ route('backend.monitoracao') }}">Monitoração</a></li>
</ul>