<?php

namespace App\Providers;

use App\Models\AttendanceSteps;
use App\Observers\AttendanceStepObserve;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale('pt-BR');

        AttendanceSteps::observe(AttendanceStepObserve::class);
    }
}
