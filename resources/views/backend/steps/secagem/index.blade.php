@extends('backend.base')
@section('title', $titulo)

@section('breadcrumb')
<div class="page-heading">
  <h1>@yield('title') ({{count($results)}})</h1>
  <ul class="breadcrumb">
    <li><a href="{{ route('backend.home') }}">Início</a></li>
    <li><a href="">@yield('title')</a></li>
  </ul>
</div>
@endsection

@section('content')
<section>
  <div class="card">
    <div class="card-body" id="content-page">
      @include('backend.steps.secagem.form')
      <hr>
      <h4>Listagem de Códigos de Barras</h4>
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover mb-0">
          <thead>
            <th width="40px">
              <div class="form-group form-check">
                <input type="checkbox" id="checkAll" value="" class="form-check-input">
              </div>
            </th>
            <th width="100px"><a href="{{ request()->fullUrlWithQuery(['column' => 'number_id', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> N°</th>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'code', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> Código de Barras</th>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'service', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> Serviço</th>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'checkin_date', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> Data Checkin</th>
            <th><a href="{{ request()->fullUrlWithQuery(['column' => 'updated_at', 'order' => $order]) }}"><i class="fa fa-sort"></i></a> Última Alteração</th>
            <th>Status</th>
          </thead>
          <tbody>
            <form class="form" id="form-table">
              {{ csrf_field() }}
              @foreach($results as $result)
              <tr>
                <td>
                  <div class="form-group form-check">
                    <input type="checkbox" name="selected[]" value="{{ $result->id }}" class="form-check-input">
                  </div>
                </td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">N° Atendimento:</span> <a href="{{ url('buscar?code='.$result->number_id) }}">{{ $result->number_id }}</a></td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Código de Barras:</span> <a href="{{ url('buscar?code='.$result->code) }}">{{ $result->code }}</a></td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Serviço:</span> {{ $result->service }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Data Checkin:</span> {{ \Carbon\Carbon::parse($result->checkin_date)->format('d/m/Y - H:i') }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Atualização:</span> {{ \Carbon\Carbon::parse($result->updated_at)->format('d/m/Y - H:i') }}</td>
                <td><span class="d-inline-block d-sm-inline-block d-md-none">Status:</span>
                  @if($result->secagem_status == 'Concluído')
                  <span class="badge badge-success"><i class="fa fa-check"></i> {{$result->secagem_status}}</span>
                  @else
                  <span class="badge badge-warning"><i class="fa fa-minus"></i> {{$result->secagem_status}}</span>
                  @endif
                </td>
              </tr>
              @endforeach
            </form>
          </tbody>
        </table>
        <div class="d-flex align-items-center mt-3">
          <div class="mr-2"><small>Com os Selecionados:</small></div>
          <div><a href="#" id="btn-delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i> Remover selecionados</a></div>
          <div class="flex-grow-1">
            {{$results->appends(request()->query())->links() }}
          </div>
        </div><!-- d-flex -->
      </div><!-- table-responsive -->

    </div><!-- card-body -->
  </div><!-- card -->
</section>
@endsection

@section('cssPage')
<link rel="stylesheet" href="/general/plugins/sweetalert/sweetalert2.min.css">
@endsection

@section('jsPage')
<script src="/general/plugins/sweetalert/sweetalert2.min.js"></script>
<script>
  $('#form-request').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
  });
  // First input focus
  $('.codeInput').last().focus();

  // Count e Limit catacteres - meta keywords
  var atual = $('.codeInput').last().val().length;
  $(".CountCaracteresName").text(atual + '/13 caracteres restantes.');

  $(document).on("input", ".codeInput:last-child", function() {
    var inicio = 0;
    var limite = 13;
    var informativo = "/" + limite + " caracteres restantes.";
    var caracteresDigitados = $(this).val().length;
    var caracteresAtuais = caracteresDigitados;
    var caracteresRestantes = limite - caracteresDigitados;

    if (caracteresRestantes <= 0) {
      var comentario = $(".codeInput:last-child").val();
      console.log(comentario);
      $(".CountCaracteresName").text('13' + informativo);
    } else {
      $(".CountCaracteresName").text(caracteresAtuais + informativo);
    }
  });


  function validCode(input) {
    var maxTotalCode = 13; //max 13 digitos
    var codebar = input.val();
    var countLenght = input.val().length;

    var inputs = $("input.codeInput")
      .map(function() {
        return $(this).val();
      }).get();

    var uniqueCodes = [];
    $.each(inputs, function(i, el) {
      if ($.inArray(el, uniqueCodes) === -1) {
        uniqueCodes.push(el);
      } else {
        $(".codeInput").each(function() {
          if (el == $(this).val()) {
            $(this).addClass('hasError').addClass('important');
            $(".codeInput.hasSuccess.hasError.important").each(function() {
              $(this).parent().parent().find('a').removeClass('hide');
            });
          }
        });
      }
    });


    if (input.val().length > maxTotalCode) {
      var newvalue = input.val().substring(0, maxTotalCode);
      input.val(newvalue);
    }

    // Aqui entra em ação quando digita os maxTotalCode
    if (countLenght == maxTotalCode) {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': "{{csrf_token()}}"
        }
      });

      var url = `{{url('checkcode/${codebar}/')}}`;
      var method = 'GET';

      $.ajax({
        url: url,
        method: method,
        success: function(codebar) {
          if (codebar) {
            $('.errocode').html('');
            input.removeClass('hasError');
            input.addClass('hasSuccess');
            input.parent().parent().find('.remove-input-code').addClass('hide');
            removeInputEmpty();
            newCode();
            proxInput();
            $(".CountCaracteresName").html('0/13 caracteres restantes');
          } else {
            $('.errocode').addClass('text-danger').html('Código não encontrado ou inválido.');
            input.addClass('hasError');
          }
        },
        error: function(xhr) {
          console.log('preciso add a class de error e não criar o novo input')
        }
      });

    }
  }

  function proxInput() {
    $('.codeInput').last().focus();
  }

  function newCode() {
    var i = parseInt($("#inputCodes input").last().attr('data-inputcode'));
    i = i + 1;
    var input = '';
    input += '<div class="d-flex row' + i + '"><div class="flex-grow-1 mr-2">';
    input += '<input type="tel" id="code' + i + '" name="code[]" data-inputcode="' + i + '" class="form-control mb-2 codeInput" placeholder="Código de Barras (13 dígitos)" value=""  oninput="validCode($(this))">';
    input += '</div><div class=""><a href="javascript:;" data-removerow="' + i + '" class="btn btn-xs btn-thema mt-1 remove-input-code hide" onClick="removeRow($(this));" data-toggle="tooltip" data-placement="right" title="Remover linha"><i class="fa fa-times"></i></a></div></div>';
    $("#inputCodes").append(input);
  }


  function removeInputEmpty() {
    var inputEmpty = $(".codeInput").filter(function() {
      return !this.value;
    }).get();

    if (inputEmpty.length) {
      $(inputEmpty).remove();
    }
  }

  function removeRow(alvo) {
    var rowRemove = '.row' + alvo.attr('data-removerow');
    $(rowRemove).remove();
    $('.tooltip').remove();
    // console.log(rowRemove);
  }





  // Button Save Forms - Create and Edit
  $(document).on('click', '#btn-salvar', function(e) {
    e.preventDefault();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': "{{csrf_token()}}"
      }
    });

    removeInputEmpty();

    let id = $(this).data('id');
    var type = $(this).data('type');

    if (type == 'edit') {
      if (id) {
        var url = `{{ url($url_action.'-update/${id}') }}`;
        var method = 'PUT';
      }
    } else {
      var url = "{{ url($url_action.'-store') }}";
      var method = 'POST';
    }
    var data = $('#form-request').serialize();
    $.ajax({
      url: url,
      data: data,
      method: method,
      success: function(data) {
        Swal.fire({
          text: data,
          icon: 'success',
          showClass: {
            popup: 'animate_animated animate_backInUp'
          },
          onClose: () => {
            // limpa campos para novo insert
            // $('#form-request')[0].reset();
            // Loading page listagem
            location.href = "{{url($url_action)}}";
          }
        });
      },
      error: function(xhr) {
        if (xhr.status === 422) {
          Swal.fire({
            text: 'Validação: ' + xhr.responseJSON,
            icon: 'warning',
            showClass: {
              popup: 'animate_animated animate_wobble'
            }
          });
        } else {
          Swal.fire({
            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
            icon: 'error',
            showClass: {
              popup: 'animate_animated animate_wobble'
            }
          });
        }
      }
    });
  });
</script>
<script>
  $("#checkAll").click(function() {
    $('input:checkbox').not(this).prop('checked', this.checked);
  });

  // Button - Delete - Not action Waiting
  $('#btn-delete').click(function(e) {
    Swal.fire({
      title: 'Deseja remover este registro?',
      text: "Você não poderá reverter isso!",
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, deletar!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: "{{url($url_action.'-delete')}}",
          method: 'DELETE',
          data: $('#form-table').serialize(),
          success: function(data) {
            // Loading page listagem
            location.href = "{{url($url_action)}}";
          },
          error: function(xhr) {
            if (xhr.status === 422) {
              Swal.fire({
                text: xhr.responseJSON,
                icon: 'warning',
                showClass: {
                  popup: 'animate_animated animate_wobble'
                }
              });
            } else {
              Swal.fire({
                text: xhr.responseJSON,
                icon: 'error',
                showClass: {
                  popup: 'animate_animated animate_wobble'
                }
              });
            }
          }
        });
      }
    });

  });
</script>
@endsection