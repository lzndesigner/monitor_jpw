@extends('auth.base')

@section('content')
<h1 class="h3 mb-3 font-weight">Acesso Restrito</h1>

<form method="POST" action="{{ route('login') }}">
    @csrf
    <label for="email" class="sr-only">{{ __('E-mail de Acesso') }}</label>
    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-mail de Acesso') }}">

    <label for="inputPassword" class="sr-only">{{ __('Senha de Acesso') }}</label>
    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Senha de Acesso') }}">

    @error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror

    @error('password')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror

    <div class="checkbox mb-3 d-none">
        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
        <label class="form-check-label" for="remember">
            {{ __('Lembrar-me') }}
        </label>
    </div>

    <button class="btn btn-lg btn-thema btn-block" type="submit">{{ __('Acessar Painel') }}</button>
    
    @if (Route::has('password.request'))
    <a class="btn btn-link d-none" href="{{ route('password.request') }}">
        {{ __('Esqueceu sua senha?') }}
    </a>
    @endif
</form>
@endsection