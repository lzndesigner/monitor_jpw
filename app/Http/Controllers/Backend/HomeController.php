<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    // Dashboard
    public function index()
    {
        $countStepCheckin = DB::table('attendance_steps')->where('checkin_status', 'Em Andamento')->count();
        $countStepLavagem = DB::table('attendance_steps')->where('lavagem_status', 'Em Andamento')->count();
        $countStepSecagem = DB::table('attendance_steps')->where('secagem_status', 'Em Andamento')->count();
        $countStepAcabamento = DB::table('attendance_steps')->where('acabamento_status', 'Em Andamento')->count();
        $countStepFinalizado = DB::table('attendance_steps')->where('finalizado_status', 'Em Andamento')->count();
        $countStepWaiting = DB::table('attendance_steps')->where('checkout_status', 'Aguardando')->count();
        $countTotal = $countStepCheckin + $countStepLavagem + $countStepSecagem + $countStepAcabamento + $countStepFinalizado + $countStepWaiting;

        $data['requests'] = [
            'countStepCheckin' => $countStepCheckin,
            'countStepLavagem' => $countStepLavagem,
            'countStepSecagem' => $countStepSecagem,
            'countStepAcabamento' => $countStepAcabamento,
            'countStepFinalizado' => $countStepFinalizado,
            'countStepWaiting' => $countStepWaiting,
            'countTotal' => $countTotal,
        ];

        return view('backend.home')->with($data['requests']);
    }

    // Monitor
    public function monitoracao()
    {
        $datenow = \Carbon\Carbon::now();
        $dateSevendays = \Carbon\Carbon::now()->subDays(7);
        $dateNinedays = \Carbon\Carbon::now()->subDays(9);
        $dateTendays = \Carbon\Carbon::now()->subDays(10);

        $getSevendays = DB::table('attendance_steps')->select('number_id')
            ->where('checkout_status', '!=', 'Concluído')
            ->where('checkout_status', '!=', 'Aguardando')
            ->orWhereNull('checkout_status')
            ->where('updated_at', '<', $dateSevendays)
            ->where('updated_at', '>', $dateNinedays)
            // ->get();
            ->groupby('number_id')->get();

        $getNinedays = DB::table('attendance_steps')->select('number_id')
            ->where('checkout_status', '!=', 'Concluído')
            ->where('checkout_status', '!=', 'Aguardando')
            ->orWhereNull('checkout_status')
            ->where('updated_at', '<', $dateNinedays)
            ->where('updated_at', '>', $dateTendays)
            // ->get();
            ->groupby('number_id')->get();

        $getTendays = DB::table('attendance_steps')->select('number_id')
            ->where('checkout_status', '!=', 'Concluído')
            ->where('checkout_status', '!=', 'Aguardando')
            ->orWhereNull('checkout_status')
            ->where('updated_at', '<', $dateTendays)
            // ->get();
            ->groupby('number_id')->get();

        $getCheckins = DB::table('attendance_steps')->select('number_id')->where('checkin_status', 'Em andamento')->groupby('number_id')->get();
        $getLavagens = DB::table('attendance_steps')->select('number_id')->where('lavagem_status', 'Em andamento')->groupby('number_id')->get();
        $getSecagens = DB::table('attendance_steps')->select('number_id')->where('secagem_status', 'Em andamento')->groupby('number_id')->get();
        $getAcabamentos = DB::table('attendance_steps')->select('number_id')->where('acabamento_status', 'Em andamento')->groupby('number_id')->get();
        $getFinalizados = DB::table('attendance_steps')->select('number_id')->where('finalizado_status', 'Em andamento')->groupby('number_id')->get();
        $getWaitings = DB::table('attendance_steps')->select('number_id')->where('checkout_status', 'Aguardando')->groupby('number_id')->get();

        $dataRequest = [
            'getSevendays'               =>  $getSevendays,
            'getNinedays'               =>  $getNinedays,
            'getTendays'               =>  $getTendays,
            'getCheckins'               =>  $getCheckins,
            'getLavagens'               =>  $getLavagens,
            'getSecagens'               =>  $getSecagens,
            'getAcabamentos'               =>  $getAcabamentos,
            'getFinalizados'               =>  $getFinalizados,
            'getWaitings'               =>  $getWaitings,
        ];

        return view('backend.monitoracao')->with($dataRequest);
    }

    // Validacao de o codigo ja existe no banco
    public function checkcode($codebar)
    {
        $result = DB::table('attendance_steps')->where('code', $codebar)->first();

        if ($result) {
            return response()->json(true, 200);
        } else {
            return response()->json(false);
        }
    }

    // Busca por numero de atendimento ou codigo de barras
    public function search()
    {
        $order = $this->request->input('order') == 'asc' ? 'desc' : 'asc';
        $column_name = null;

        if ($this->request->input('column')) {
            $column = $this->request->input('column');
            $column_name = "$column $order";
        } else {
            $column_name = "checkin_date desc";
        }

        $field = $this->request->input('field') ? $this->request->input('field') : 'number_id';
        $operador = $this->request->input('operador') ? $this->request->input('operador') : 'like';
        $value = $this->request->input('value') ? $this->request->input('value') : '';

        if ($field == 'data' || $field == 'dataini' || $field == 'datafim') {
            $value = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }

        if ($operador == 'like') {
            $newValue = "'%$value%'";
        } else {
            $newValue = "'$value'";
        }

        $inputCode = $this->request->input('code');
        $countCode = strlen($inputCode);

        if ($countCode < 13) {
            $infoAttendance = DB::table('attendances')
                ->where('number', $inputCode)
                ->first();

            $codes = DB::table('attendance_steps')
                ->orderByRaw("$column_name")
                ->whereraw("$field $operador $newValue")
                ->where('number_id', $inputCode)
                ->paginate(10);

            return view('backend.search-number', compact('infoAttendance', 'codes', 'order'));
        } else {
            $result = DB::table('attendance_steps')
                ->where('code', $inputCode)
                ->first();

            if ($result) {
                $infoAttendance = DB::table('attendances')
                    ->where('number', $result->number_id)
                    ->first();
            } else {
                $infoAttendance = null;
            }

            return view('backend.search-codebar', compact('result', 'infoAttendance', 'order'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroyCode()
    {
        $data = $this->request->all();

        if (!isset($data['selected'])) {
            return response()->json('Selecione ao menos um registro', 422);
        }

        try {
            foreach ($data['selected'] as $result) {
                DB::table('attendance_steps')->where('id', $result)->delete();
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json($this->request->all(), 200);
    }
}
