@extends('auth.base')

@section('content')
<h1 class="h3 mb-3 font-weight-normal">Recuperar Senha</h1>


@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif

<form method="POST" action="{{ route('password.email') }}">
    @csrf
    <label for="email" class="sr-only">{{ __('E-mail de Acesso') }}</label>
    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-mail de Acesso') }}">

    @error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror

    <button class="btn btn-lg btn-primary btn-block mt-3" type="submit">{{ __('Enviar E-mail') }}</button>

    <a href="/login">Faça seu login</a>
</form>
@endsection