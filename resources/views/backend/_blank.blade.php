@extends('backend.base')
@section('title', 'Em branco')

@section('breadcrumb')
<div class="page-heading">
  <h1>Pagina Inicial</h1>
  <ul class="breadcrumb">
    <li><a href="">Início</a></li>
    <li><a href="">@yield('title')</a></li>
  </ul>
</div>
@endsection

@section('content')
<section>
  <div class="card">
    <div class="card-body">
      <small>Conteúdo</small>
    </div>
  </div>
</section>
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection